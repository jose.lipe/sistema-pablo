<?php
use Livro\Control\Page;
use Livro\Control\Action;
use Livro\Widgets\Form\Form;
use Livro\Widgets\Form\Entry;
use Livro\Widgets\Form\Combo;
use Livro\Widgets\Form\Label;
use Livro\Widgets\Form\Button;
use Livro\Widgets\Container\VBox;
use Livro\Widgets\Datagrid\Datagrid;
use Livro\Widgets\Datagrid\DatagridColumn;
use Livro\Widgets\Datagrid\DatagridAction;
use Livro\Widgets\Dialog\Message;
use Livro\Widgets\Dialog\Question;
use Livro\Database\Transaction;
use Livro\Database\Repository;
use Livro\Traits\DeleteTrait;
use Livro\Traits\ReloadTrait;
use Livro\Traits\SaveTrait;
use Livro\Traits\EditTrait;
use Livro\Widgets\Wrapper\DatagridWrapper;
use Livro\Widgets\Wrapper\FormWrapper;
use Livro\Widgets\Container\Panel;

class FabricantesFormList extends Page
{
	private $form, $datagrid, $loaded;

	use EditTrait;
	use DeleteTrait;
	use ReloadTrait{
		onReload as onReloadTrait;
	}
	use SaveTrait{
		onSave as onSaveTrait;
	}

	public function __construct()
	{
		parent::__construct();

		$this->connection = 'livro';
		$this->activeRecord = 'Fabricante';

		// instancia um formulário
		$this->form = new FormWrapper(new Form('form_fabricantes'));

		//cria os campos do formulário
		$codigo = new Entry('id');
		$nome = new Entry('nome');
		$site = new Entry('site');

		$codigo->setEditable(FALSE);

		$this->form->addField('Código', $codigo, 40);
		$this->form->addField('Nome', $nome, 300);
		$this->form->addField('Site', $site, 300);

		$this->form->addAction('Salvar', new Action(array($this, 'onSave')));
		$this->form->addAction('Limpar', new Action(array($this, 'onEdit')));

		// instancia a Datagrid
		$this->datagrid = new DatagridWrapper(new Datagrid);

		// instancia as colunas da Datagrid
		$codigo = new DatagridColumn('id', 'Código', 'right', 50);
		$nome = new DatagridColumn('nome', 'Nome', 'left', 180);
		$site = new DatagridColumn('site', 'Site', 'left', 180);

		// adiciona as colunas à Datagrid
		$this->datagrid->addColumn($codigo);
		$this->datagrid->addColumn($nome);
		$this->datagrid->addColumn($site);

		// instancia duas ações da Datagrid
		$action1 = new DatagridAction(array($this, 'onEdit'));
		$action1->setLabel('Editar');
		$action1->setImage('ico_edit.png');
		$action1->setField('id');

		$action2 = new DatagridAction(array($this, 'onDelete'));
		$action2->setLabel('Delete');
		$action2->setImage('ico_delete.png');
		$action2->setField('id');

		// adiciona as ações à Datagrid
		$this->datagrid->addAction($action1);
		$this->datagrid->addAction($action2);

		// cria o modelo da Datagrid, montando sua estrutura
		$this->datagrid->createModel();

		$panel = new Panel('Cidades');
		$panel->add($this->form);

		$panel2 = new Panel();
		$panel2->add($this->datagrid);

		// monta a página por meio de uma tabela
		$box = new VBox;
		$box->style = 'display:block';
		$box->add($panel);
		$box->add($panel2);

		parent::add($box);
	}

	public function onSave()
	{
		$this->onSaveTrait();
		$this->onReload();
	}

	public function onReload()
	{
		$this->onReloadTrait();
		$this->loaded = true;
	}

	public function show()
	{
		// se a listagem ainda não foi carregada
		if (!$this->loaded)
		{
			$this->onReload();
		}
		parent::show();
	}
}