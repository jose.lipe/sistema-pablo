<?php
use Livro\Control\Page;
use Livro\Control\Action;
use Livro\Widgets\Form\Form;
use Livro\Widgets\Form\Entry;
use Livro\Widgets\Container\VBox;
use Livro\Widgets\Datagrid\Datagrid;
use Livro\Widgets\Datagrid\DatagridColumn;
use Livro\Widgets\Datagrid\DatagridAction;
use Livro\Widgets\Dialog\Message;
use Livro\Widgets\Dialog\Question;
use Livro\Widgets\Wrapper\FormWrapper;
use Livro\Widgets\Wrapper\DatagridWrapper;
use Livro\Database\Transaction;
use Livro\Database\Repository;
use Livro\Database\Criteria;
use Livro\Database\Filter;

class PessoasList extends Page
{
	private $form; // formulário de buscas
	private $datagrid; // listagem
	private $loaded;

	public function __construct()
	{
		parent::__construct();

		// instancia um formulário de buscas
		$this->form = new FormWrapper(new Form('form_busca_pessoas'));
		$nome = new Entry('nome');
		$this->form->addField('Nome', $nome, 300);
		$this->form->addAction('Buscar', new Action(array($this, 'onReload')));
		$this->form->addAction('Novo', new Action(array(new PessoasForm, 'onEdit')));

		// instancia objeto Datagrid
		$this->datagrid = new DatagridWrapper(new Datagrid);

		// instancia as colunas da Datagrid
		$codigo = new DatagridColumn('id', 'Código', 'right', 50);
		$nome = new DatagridColumn('nome', 'Nome', 'left', 200);
		$endereco = new DatagridColumn('endereco', 'Endereço', 'left', 200);
		$cidade = new DatagridColumn('nome_cidade', 'Cidade', 'left', 140);

		// adiciona as colunas à Datagrid
		$this->datagrid->addColumn($codigo);
		$this->datagrid->addColumn($nome);
		$this->datagrid->addColumn($endereco);
		$this->datagrid->addColumn($cidade);

		// instancia duas ações da Datagrid
		$action1 = new DatagridAction(array(new PessoasForm, 'onEdit'));
		$action1->setLabel('Editar');
		$action1->setImage('ico_edit.png');
		$action1->setField('id');

		$action2 = new DatagridAction(array($this, 'onDelete'));
		$action2->setLabel('Deletar');
		$action2->setImage('ico_delete.png');
		$action2->setField('id');

		// adiciona as ações à Datagrid
		$this->datagrid->addAction($action1);
		$this->datagrid->addAction($action2);

		// cria o modelo da Datagrid, montando sua estrutura
		$this->datagrid->createModel();

		// monta a página por meio de uma caixa
		$box = new VBox;
		$box->style = 'display:block';
		$box->add($this->form);
		$box->add($this->datagrid);

		parent::add($box);
	}

	public function onReload()
	{
		Transaction::open('livro'); // inicia transação com o BD
		$repository = new Repository('Pessoa');

		// cria um critério de seleção de dados
		$criteria = new Criteria;
		$criteria->setProperty('order', 'id');

		// obtém od dados do formulário de buscas
		$dados = $this->form->getData();

		// verifica se o usuário preencheu o formulário
		if ($dados->nome)
		{
			// filtra pelo nome da pessoa
			$criteria->add(new Filter('nome', 'like', "%{$dados->nome}%"));
		}

		// carrega os produtos que satisfazem o critério
		$pessoas = $repository->load($criteria);
		$this->datagrid->clear();
		if ($pessoas)
		{
			foreach ($pessoas as $pessoa) {
				// adiciona o objeto à Datagrid
				$this->datagrid->addItem($pessoa);
			}
		}
		// finaliza a transação
		Transaction::close();
		$this->loaded = true;
	}

	public function onDelete($param)
	{
		$id = $param['id']; // obtém o parâmetro id
		$action1 = new Action(array($this, 'Delete'));
		$action1->setParameter('id', $id);

		new Question('Deseja realmente excluir o registro?', $action1);
	}

	public function Delete($param)
	{
		try{
			$id = $param['id']; // obtém a chave
			Transaction::open('livro'); // inicia transação com o banco de dados
			$pessoa = Pessoa::find($id);
			$pessoa->delete(); // deleta objeto do banco de dados
			Transaction::close(); // recarrega a transação
			$this->onReload(); // recarrega a datagrid
			new Message('info', "Registro excluído com sucesso");
		}
		catch (Exception $e){
			new Message('error', $e->getMessage());
		}
	}

	public function show()
	{
		// se a listagem ainda não foi carregada
		if (!$this->loaded)
		{
			$this->onReload();
		}
		parent::show();
	}
}